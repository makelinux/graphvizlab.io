---
title: dot
weight: 1
description: >
  "hierarchical" or layered drawings of directed graphs.
---

`dot` is the default tool to use if edges have directionality.

The layout algorithm aims edges in the same direction (top to bottom, or left
to right) and then attempts to avoid edge crossings and reduce edge length.

<p style="text-align: center;">
  <a href="/Gallery/directed/cluster.html">
    <img src="/Gallery/directed/cluster.svg">
  </a>
</p>

Dot-specific attributes:
- [clusterrank]({{< ref "../attrs/clusterrank.md" >}})
- [compound]({{< ref "../attrs/compound.md" >}})
- [constraint]({{< ref "../attrs/constraint.md" >}})
- [group]({{< ref "../attrs/group.md" >}})
- [lhead]({{< ref "../attrs/lhead.md" >}})
- [ltail]({{< ref "../attrs/ltail.md" >}})
- [mclimit]({{< ref "../attrs/mclimit.md" >}})
- [minlen]({{< ref "../attrs/minlen.md" >}})
- [newrank]({{< ref "../attrs/newrank.md" >}})
- [nodesep]({{< ref "../attrs/nodesep.md" >}})
- [nslimit]({{< ref "../attrs/nslimit.md" >}})
- [nslimit1]({{< ref "../attrs/nslimit1.md" >}})
- [ordering]({{< ref "../attrs/ordering.md" >}})
- [rank]({{< ref "../attrs/rank.md" >}})
- [rankdir]({{< ref "../attrs/rankdir.md" >}})
- [ranksep]({{< ref "../attrs/ranksep.md" >}})
- [remincross]({{< ref "../attrs/remincross.md" >}})
- [samehead]({{< ref "../attrs/samehead.md" >}})
- [sametail]({{< ref "../attrs/sametail.md" >}})
- [searchsize]({{< ref "../attrs/searchsize.md" >}})
- [showboxes]({{< ref "../attrs/showboxes.md" >}})

[PDF Manual](/pdf/dot.1.pdf)
